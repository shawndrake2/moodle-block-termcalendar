<?php

require_once("{$CFG->libdir}/formslib.php");
require_once("lib.php");

class term_calendar_form extends moodleform {

    function definition() {

        global $DB, $CFG, $OUTPUT;

		$termid = optional_param('termid', false, PARAM_INT);

        $mform =& $this->_form;
        $extdb = get_config('term_calendar', 'extdb');

        if($extdb == 0) {

            $mform->addElement('header', 'addtermheader', get_string('addtermheader', 'block_term_calendar'));
            //Term Name
            $mform->addElement('text', 'name', get_string('mapnamelabel', 'block_term_calendar'));
            $mform->setType('name', PARAM_TEXT);
            $mform->addRule('name', get_string('mapnamelabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required', null, 'client', true);
            //Start Date
            $sd = $mform->addElement('date_selector', 'startdate', get_string('mapstartlabel', 'block_term_calendar'));
            $mform->setType('startdate', PARAM_INT);
            $mform->addRule('startdate', get_string('mapstartlabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required');
            //End Date
            $mform->addElement('date_selector', 'enddate', get_string('mapendlabel', 'block_term_calendar'));
            $mform->setType('enddate', PARAM_INT);
            $mform->addRule('enddate', get_string('mapendlabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required');
			//Check to see if this is an edit
			if($termid) {
				if($terminfo = $DB->get_record('block_term_calendar', array('id' => $termid))) {
					$mform->setDefault('name', $terminfo->name);
					$mform->setDefault('startdate', $terminfo->startdate);
					$mform->setDefault('enddate', $terminfo->enddate);
					//Action Buttons
					$this->add_action_buttons(true, get_string('edittermsubmit', 'block_term_calendar'));
				}
			}else{
				//Action Buttons
	            $this->add_action_buttons(true, get_string('addtermsubmit', 'block_term_calendar'));
			}
        }

        $mform->addElement('header', 'showterminfo', get_string('showterminfoheader', 'block_term_calendar'));
		$today = time();
		if($terms = $DB->get_records_select('block_term_calendar', 'enddate > '.$today))
		{
			$t = new html_table();
			$t->head = array(get_string('mapnamelabel', 'block_term_calendar'), get_string('mapstartlabel', 'block_term_calendar'), get_string('mapendlabel', 'block_term_calendar'), '');
			foreach($terms as $term) {
				$eicon = '<a title="'.get_string('edit').'" href="'.$CFG->wwwroot.'/blocks/term_calendar/view.php?termid='.$term->id.'"><img class="iconsmall" src="'.$OUTPUT->pix_url('t/edit').'" alt="'.get_string('edit').'" /></a>';
				$dicon = '<a title="'.get_string('delete').'" href="'.$CFG->wwwroot.'/blocks/term_calendar/delete.php?termid='.$term->id.'&amp;name='.$term->name.'"><img class="iconsmall" src="'.$OUTPUT->pix_url('t/delete').'" alt="'.get_string('delete').'" /></a>';
				unset($term->id);
				$cell1 = new html_table_cell();
				$cell1->text = $term->name;
				$cell2 = new html_table_cell();
				$cell2->text = date('F j, Y', $term->startdate);
				$cell3 = new html_table_cell();
				$cell3->text = date('F j, Y', $term->enddate);
				$cell4 = new html_table_cell();
				$cell4->text = $eicon.$dicon;
				$row = new html_table_row();
				if($today >= $term->startdate && $today <= $term->enddate) {
					$row->style = 'font-weight: bold; background-color: #e2e2e2;';
				}
				$row->cells = array($cell1, $cell2, $cell3, $cell4);
				$termarray[] = $row;
			}
			$t->data = $termarray;
			$mform->addElement('html', html_writer::table($t));
		}else{
			$mform->addElement('html', get_string('noterms', 'block_term_calendar'));
		}
    }

    function validation($data, $files) {
        $errors= array();

        if ($data['enddate'] <= $data['startdate']){
            $errors['startdate'] = get_string('dateerror', 'block_term_calendar');
        }

        return $errors;
    }
}