// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package   block_term_calendar
 * @copyright 2010 onwards Shawn Drake (http://shawndrake.net)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

M.block_term_calendar = {};

M.block_term_calendar.init = function(Y)  {
	var toggle = function(status, link) {
        if(!status) {
            link.hide();
        }else{
            link.show();
        }
        /*		Y.one('#id_s_term_calendar_dbhost').set('disabled', status);
		Y.one('#id_s_term_calendar_dbusername').set('disabled', status);
		Y.one('#id_s_term_calendar_dbuserpassword').set('disabled', status);
		Y.one('#id_s_term_calendar_dbuserpasswordunmask').set('disabled', status);
		Y.one('#id_s_term_calendar_dbname').set('disabled', status);
		Y.one('#id_s_term_calendar_dbtable').set('disabled', status);
		Y.one('#id_s_term_calendar_mapname').set('disabled', status);
		Y.one('#id_s_term_calendar_mapstart').set('disabled', status);
		Y.one('#id_s_term_calendar_mapend').set('disabled', status);*/
	}
    var status = Y.one('#admin-extdb').get('checked');
    var link = Y.one('#dbconfiglink');

	toggle(status, link);
	
	Y.on('change', function(e) {
		var status = e.target.get('checked');
		toggle(status);
	}, '#admin-extdb' );
};
