<?php

require_once('../../config.php');
require_once('term_calendar_form.php');

global $DB, $OUTPUT, $PAGE;

$PAGE->set_context(context_system::instance());
$PAGE->set_url('/blocks/term_calendar/view.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string('addterm', 'block_term_calendar'));

$settingsnode = $PAGE->settingsnav->add(get_string('term_calendar', 'block_term_calendar'));
$editurl = new moodle_url('/blocks/term_calendar/view.php');
$editnode = $settingsnode->add(get_string('addterm', 'block_term_calendar'), $editurl);
$editnode->make_active();

$term_calendar = new term_calendar_form();

if($term_calendar->is_cancelled()) {
    // Cancelled forms redirect to the course main page.
    redirect($CFG->wwwroot);
} else if ($fromform = $term_calendar->get_data()) {
    $url = new moodle_url('/blocks/term_calendar/view.php');
    if($DB->record_exists('block_term_calendar', array('name'=>$fromform->name))) {
        $current = $DB->get_record('block_term_calendar', array('name'=>$fromform->name),'id');
        $fromform->id = $current->id;
        if (!$DB->update_record('block_term_calendar',$fromform)) {
            print_error('inserterror', 'block_term_calendar');
        }
        //NEED TO ADD CODE TO LET USER KNOW RECORD WAS UPDATED

    }else{
        if (!$DB->insert_record('block_term_calendar', $fromform)) {
            print_error('inserterror', 'block_term_calendar');
        }
        //NEED TO ADD CODE TO LET USER KNOW RECORD WAS ADDED
    }
    redirect($url);
} else {
    // form didn't validate or this is the first display
    if(get_config('term_calendar', 'extdb') == 1) {
        require_once('lib.php');
        syncdb();
    }
    $site = get_site();
    echo $OUTPUT->header();
    $term_calendar->display();
    echo $OUTPUT->footer();
}
?>