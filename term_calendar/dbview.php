<?php

require_once('../../config.php');
require_once('term_calendar_dbform.php');

global $DB, $OUTPUT, $PAGE;

$PAGE->set_context(context_system::instance());
$PAGE->set_url('/blocks/term_calendar/dbview.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string('configdb', 'block_term_calendar'));

$settingsnode = $PAGE->settingsnav->add(get_string('term_calendar', 'block_term_calendar'));
$editurl = new moodle_url('/blocks/term_calendar/dbview.php');
$editnode = $settingsnode->add(get_string('configdb', 'block_term_calendar'), $editurl);
$editnode->make_active();

$term_calendar = new term_calendar_form();

if($term_calendar->is_cancelled()) {
    // Cancelled forms redirect to the course main page.
    $url = new moodle_url('/admin/settings.php?section=blocksettingterm_calendar');
    redirect($url);
} else if ($fromform = $term_calendar->get_data()) {
    $url = new moodle_url('/blocks/term_calendar/dbview.php');
    set_config('dbhost', $fromform->dbhost, 'term_calendar');
    set_config('dbusername', $fromform->dbusername, 'term_calendar');
    set_config('dbuserpassword', $fromform->dbuserpassword, 'term_calendar');
    set_config('dbname', $fromform->dbname, 'term_calendar');
    set_config('dbtable', $fromform->dbtable, 'term_calendar');
    set_config('mapname', $fromform->mapname, 'term_calendar');
    set_config('mapstart', $fromform->mapstart, 'term_calendar');
    set_config('mapend', $fromform->mapend, 'term_calendar');
    redirect($url);
    /////////////////////////////////////////////////////
    // CONFIRM CHANGES ARE SAVED
    ////////////////////////////////////////////////////
    // ALSO NEED TO CONFIRM SETTINGS MAKE CONNECTION TO DB
    /////////////////////////////////////////////////////
} else {
    // form didn't validate or this is the first display
    $site = get_site();
    echo $OUTPUT->header();
    $term_calendar->display();
    echo $OUTPUT->footer();
}
?>