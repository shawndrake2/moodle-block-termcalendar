<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage term_calendar
 * @copyright  2010 onwards Shawn Drake (http://shawndrake.net)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function dbcon($extdb) {
	
    if($extdb['host'] == '' || $extdb['user'] == '' || $extdb['name'] == '') {
        return get_string('configerror', 'block_term_calendar');
    }

	$mysqli = new mysqli($extdb['host'], $extdb['user'], $extdb['pass'], $extdb['name']);

    return $mysqli;
}

function is_configured() {
    if(get_config('term_calendar', 'extdb') == 1) {
        if(!get_config('term_calendar', 'dbhost') || !get_config('term_calendar', 'dbusername') ||
            !get_config('term_calendar', 'dbname') || !get_config('term_calendar', 'dbtable') ||
            !get_config('term_calendar', 'mapname') || !get_config('term_calendar', 'mapstart') ||
            !get_config('term_calendar', 'mapend')) {
            return false;
        }else{
            return true;
        }
    }

    return true;
}

function syncdb() {
	
	global $DB;
		
	$extdb = array(
				'host' => get_config('term_calendar', 'dbhost'),
				'user' => get_config('term_calendar', 'dbusername'),
				'pass' => get_config('term_calendar', 'dbuserpassword'),
				'name' => get_config('term_calendar', 'dbname'),
				'table' => get_config('term_calendar', 'dbtable')
				);
    $mappedfields = array(
                        'name' => get_config('term_calendar', 'mapname'),
                        'startdate' => get_config('term_calendar', 'mapstart'),
                        'enddate' => get_config('term_calendar', 'mapend')
                        );

    $mysqli = dbcon($extdb);

    if ($mysqli->connect_error) {
        return 'Connect Error ('.$mysqli->connect_errno.') '.$mysqli->connect_error;
    }else{
        //SYNC DATABASES HERE
		$today = time();
		if($edb = $mysqli->query('SELECT '.$mappedfields['name'].', '.$mappedfields['startdate'].', '.$mappedfields['enddate'].' FROM '.$extdb['table'].' WHERE '.$mappedfields['enddate'].'>'.$today)) {

			$mdb = $DB->get_records('block_term_calendar');
			$temp = array();
			
			while($obj = $edb->fetch_object()) {
				$obj->id = null;
				$temp[] = $obj;
			}
			
			foreach($temp as $tmp) {
				$updated = false;
				$newdb = new stdClass();
				$newdb-> id = null;
				$newdb->name = $tmp->{$mappedfields['name']};
				$newdb->startdate = $tmp->{$mappedfields['startdate']};
				$newdb->enddate = $tmp->{$mappedfields['enddate']};
				foreach($mdb as $db) {
					if($tmp->{$mappedfields['name']} == $db->name) {
						$newdb->id = $db->id;
						if(!$DB->update_record('block_term_calendar', $newdb)) {
							print_error('syncerror', 'block_term_calendar');
						}
						$updated = true;
						break;
					}
				}
				if(!$updated) {
					if (!$DB->insert_record('block_term_calendar', $newdb)) {
						print_error('syncerror', 'block_term_calendar');
					}
				}
			}
			
		}
    }
}