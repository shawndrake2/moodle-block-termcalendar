<?php

require_once('../../config.php');
 
$termid = required_param('termid', PARAM_INT);
$name = required_param('name', PARAM_TEXT);
$confirm = optional_param('confirm', 0, PARAM_INT);

$back = new moodle_url('/blocks/term_calendar/view.php');
 
if(!$term = $DB->get_record('block_term_calendar', array('id' => $termid))) {
    print_error('nopage', 'block_term_calendar', $back);
}
 
$site = get_site();
$PAGE->set_url('/blocks/term_calendar/view.php');
$heading = get_string('editterminfo', 'block_term_calendar');
$PAGE->set_heading($heading);
echo $OUTPUT->header();

if (!$confirm) {
    $optionsno = $back;
    $optionsyes = new moodle_url('/blocks/term_calendar/delete.php', array('termid' => $termid,'name' => $name, 'confirm' => 1, 'sesskey' => sesskey()));
    echo $OUTPUT->confirm(get_string('confirmdelete', 'block_term_calendar').$name.'?', $optionsyes, $optionsno);
} else {
    if (confirm_sesskey()) {
       if (!$DB->delete_records('block_term_calendar', array('id' => $termid))) {
            print_error('deleteerror', 'block_term_calendar', $back);
       }
    } else {
        print_error('confirmsesskeybad');
    }
    redirect($back);
}	
echo $OUTPUT->footer();
