<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package   block_term_calendar
 * @copyright 2010 onwards Shawn Drake (http://shawndrake.net)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if(!$term = get_config('term_calendar', 'mapterm')) {
	$term = 'Term';
}

// Language Strings
$string['pluginname'] = 'Term Calendar';
$string['term_calendar'] = 'Term Calendar';
$string['term_calendar:addinstance'] = 'Add a new Term Calendar block';

$string['addterm'] = 'View '.ucfirst(strtolower($term)).' Information';
$string['addtermheader'] = 'Add New '.ucfirst(strtolower($term));
$string['addtermsubmit'] = 'Add';
$string['admin'] = 'Admin Settings';
$string['configdb'] = 'Configure Database';
$string['configerror'] = 'Database configuration not complete.';
$string['confirmdelete'] = 'Are you sure you want to delete ';
$string['currentterm'] = 'Current '.ucfirst(strtolower($term));
$string['dateerror'] = 'Start Date must come before End Date';
$string['dbhead2label'] = 'Enter your external database information below';
$string['dbhostdef'] = '';
$string['dbhostlabel'] = 'Database Host';
$string['dbinfolabel'] = 'Database Information';
$string['dbinfodesc'] = 'This block uses the Moodle Database by default. If your term calendar is stored in an external database, however, enter your database information here and the Moodle database will be updated using this external term information. Be aware that if you connect to an external database, any new term information must be loaded into that database and the block will not give you the option to add or edit dates.';
$string['dbnamedef'] = '';
$string['dbnamelabel'] = 'Database Name';
$string['dbtablelabel'] = 'Database table';
$string['dbtabledef'] = '';
$string['dbusernamedef'] = '';
$string['dbusernamelabel'] = 'Database User';
$string['dbuserpassdef'] = '';
$string['dbuserpasslabel'] = 'Database Password';
$string['deleteerror'] = 'Error deleting '.strtolower($term).'.';
$string['editterminfo'] = 'Edit Term Information';
$string['edittermsubmit'] = 'Update';
$string['extdblabel'] = 'External Database';

// Add info to link to Database Configuration page
$dbinfo = new moodle_url('/blocks/term_calendar/dbview.php');
$url = html_writer::link($dbinfo, $string['configdb'], array("id"=>"dbconfiglink"));

$string['extdbdesc'] = 'Choose whether this block uses an external database or not for term information.<br />'.$url;
$string['extdbdef'] = 0;
$string['extdberror'] = 'You are not using an external database';
$string['fieldmapheadlabel'] = 'Map database table fields';
$string['invalidcourse'] = 'You are not enrolled in this course.';
$string['inserterror'] = 'There was a problem inserting your data. Data not saved.';
$string['isrequired'] = ' is required.';
$string['mapnamelabel'] = 'Term Name';
$string['mapnamedef'] = '';
$string['mapstartlabel'] = 'Start Date';
$string['mapstartdef'] = '';
$string['maptermheaddesc'] = '';
$string['maptermheadlabel'] = 'Language Variables';
$string['maptermlabel'] = 'Term';
$string['maptermdesc'] = 'Your name for "Term"';
$string['maptermdef'] = 'Term';
$string['mapendlabel'] = 'End Date';
$string['mapenddef'] = '';
$string['nopage'] = $term.' does not exist.';
$string['noterms'] = 'There is no '.strtolower($term).' information to show.';
$string['showterminfoheader'] = 'Upcoming '.ucfirst(strtolower($term)).' Information';
$string['syncerror'] = 'Database sync failed.';
$string['term_calendar:viewpages'] = 'View Term Calendar Pages';
$string['term_calendar:managepages'] = 'Manage Term Calendar Pages';