<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package   block_term_calendar
 * @copyright 2010 onwards Shawn Drake (http://shawndrake.net)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

		$settings->add(new admin_setting_heading( 
					'term_calendar/maptermhead',
					get_string('maptermheadlabel', 'block_term_calendar'),
                    get_string('maptermheaddesc', 'block_term_calendar')
	));
	
		$settings->add(new admin_setting_configtext(
					'term_calendar/mapterm', 
					get_string('maptermlabel', 'block_term_calendar'),
					get_string('maptermdesc', 'block_term_calendar'), 
					get_string('maptermdef', 'block_term_calendar')
	));
	
	// Database settings
		$settings->add(new admin_setting_heading( 
					'term_calendar/dbinfo',
					get_string('dbinfolabel', 'block_term_calendar'),
					get_string('dbinfodesc', 'block_term_calendar')
	));
	
		$settings->add(new admin_setting_configcheckbox(
					'term_calendar/extdb', 
					get_string('extdblabel', 'block_term_calendar'),
					get_string('extdbdesc', 'block_term_calendar'), 
					get_string('extdbdef', 'block_term_calendar')
	));

    ///////////////////////////////
    //NEED TO SKIP THE CONFIGURE LINK ON INSTALL
    ///////////////////////////////
    //ADD JS CODE TO DISABLE CONFIGURE LINK WHEN CHECKBOX IS EMPTY
    ///////////////////////////////
//    $PAGE->requires->js_init_call('M.block_term_calendar.init');
}
