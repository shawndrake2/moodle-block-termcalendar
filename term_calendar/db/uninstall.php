<?php


// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package   block_term_calendar
 * @copyright 2010 onwards Shawn Drake (http://shawndrake.net)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * This is called at the beginning of the uninstallation process to give the module
 * a chance to clean-up its hacks, bits etc. where possible.
 *
 * @return bool true if success
 */
function xmldb_block_term_calendar_uninstall() {

    // Clean up config_plugin table
    unset_config('mapterm', 'term_calendar');
    unset_config('extdb', 'term_calendar');
    unset_config('dbhost', 'term_calendar');
    unset_config('dbusername', 'term_calendar');
    unset_config('dbuserpassword', 'term_calendar');
    unset_config('dbname', 'term_calendar');
    unset_config('dbtable', 'term_calendar');
    unset_config('mapname', 'term_calendar');
    unset_config('mapstart', 'term_calendar');
    unset_config('mapend', 'term_calendar');
	
    return true;
}
