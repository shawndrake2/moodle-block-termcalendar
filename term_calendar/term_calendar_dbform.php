<?php

require_once("{$CFG->libdir}/formslib.php");
require_once("lib.php");

class term_calendar_form extends moodleform {

    function definition() {

        global $DB;

        $mform =& $this->_form;
        $extdb = get_config('term_calendar', 'extdb');

        if($extdb == 0) {

			$url = new moodle_url('/admin/settings.php?section=blocksettingterm_calendar');
			redirect($url, get_string('extdberror', 'block_term_calendar'), 5);

		}else {	
			
			$mform->addElement('header', 'dbhead2', get_string('dbhead2label', 'block_term_calendar'));
            $mform->addElement('text', 'dbhost', get_string('dbhostlabel', 'block_term_calendar'));
            $mform->setType('dbhost', PARAM_TEXT);
			$mform->addRule('dbhost', get_string('dbhostlabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required', null, 'client');
            if($host = get_config('term_calendar', 'dbhost')) {
                $mform->setDefault('dbhost', $host);
            }else{
                $mform->setDefault('dbhost', get_string('dbhostdef', 'block_term_calendar'));
            }

            $mform->addElement('text', 'dbusername', get_string('dbusernamelabel', 'block_term_calendar'));
            $mform->setType('dbusername', PARAM_TEXT);
			$mform->addRule('dbusername', get_string('dbusernamelabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required', null, 'client');
            if($dbuser = get_config('term_calendar', 'dbusername')) {
                $mform->setDefault('dbusername', $dbuser);
            }else{
                $mform->setDefault('dbusername', get_string('dbusernamedef', 'block_term_calendar'));
            }

            $mform->addElement('passwordunmask', 'dbuserpassword', get_string('dbuserpasslabel', 'block_term_calendar'));
            $mform->setType('dbuserpassword', PARAM_RAW);
            if($dbuserpass = get_config('term_calendar', 'dbuserpassword')) {
                $mform->setDefault('dbuserpassword', $dbuserpass);
            }else{
                $mform->setDefault('dbuserpassword', get_string('dbuserpassdef', 'block_term_calendar'));
            }

            $mform->addElement('text', 'dbname', get_string('dbnamelabel', 'block_term_calendar'));
            $mform->setType('dbname', PARAM_TEXT);
			$mform->addRule('dbname', get_string('dbnamelabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required', null, 'client');
            if($dbname = get_config('term_calendar', 'dbname')) {
                $mform->setDefault('dbname', $dbname);
            }else{
                $mform->setDefault('dbname', get_string('dbnamedef', 'block_term_calendar'));
            }

            $mform->addElement('text', 'dbtable', get_string('dbtablelabel', 'block_term_calendar'));
            $mform->setType('dbtable', PARAM_TEXT);
			$mform->addRule('dbtable', get_string('dbtablelabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required', null, 'client');
            if($dbtable = get_config('term_calendar', 'dbtable')) {
                $mform->setDefault('dbtable', $dbtable);
            }else{
                $mform->setDefault('dbtable', get_string('dbtabledef', 'block_term_calendar'));
            }

			$mform->addElement('header', 'fieldmaphead', get_string('fieldmapheadlabel', 'block_term_calendar'));
            $mform->addElement('text', 'mapname', get_string('mapnamelabel', 'block_term_calendar'));
            $mform->setType('mapname', PARAM_TEXT);
			$mform->addRule('mapname', get_string('mapnamelabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required', null, 'client');
            if($mapname = get_config('term_calendar', 'mapname')) {
                $mform->setDefault('mapname', $mapname);
            }else{
                $mform->setDefault('mapname', get_string('mapnamedef', 'block_term_calendar'));
            }

            $mform->addElement('text', 'mapstart', get_string('mapstartlabel', 'block_term_calendar'));
            $mform->setType('mapstart', PARAM_TEXT);
			$mform->addRule('mapstart', get_string('mapstartlabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required', null, 'client');
            if($mapstart = get_config('term_calendar', 'mapstart')) {
                $mform->setDefault('mapstart', $mapstart);
            }else{
                $mform->setDefault('mapstart', get_string('mapstartdef', 'block_term_calendar'));
            }

            $mform->addElement('text', 'mapend', get_string('mapendlabel', 'block_term_calendar'));
            $mform->setType('mapend', PARAM_TEXT);
			$mform->addRule('mapend', get_string('mapendlabel', 'block_term_calendar').get_string('isrequired', 'block_term_calendar'), 'required', null, 'client');
            if($mapend = get_config('term_calendar', 'mapend')) {
                $mform->setDefault('mapend', $mapend);
            }else{
                $mform->setDefault('mapend', get_string('mapenddef', 'block_term_calendar'));
            }

            //Action Buttons
            $this->add_action_buttons();
			
	        $adminsettings = new moodle_url('/admin/settings.php?section=blocksettingterm_calendar');
			$mform->addElement('html', html_writer::start_tag('p')
										.html_writer::link($adminsettings, get_string('admin', 'block_term_calendar'))
										.html_writer::end_tag('p'));

		}
    }

/*    function validation($data, $files) {
        $errors= array();

        if ($data['enddate'] <= $data['startdate']){
            $errors['startdate'] = get_string('dateerror', 'block_term_calendar');
        }

        return $errors;
    }
*/}