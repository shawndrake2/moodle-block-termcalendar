<?php

class block_term_calendar extends block_base {
	
    function init() {		
        $this->title = get_string('term_calendar', 'block_term_calendar');
    }
	
	public function instance_allow_multiple() {
		return false;
	}
	
	function has_config() {
		return true;
	}
	
	public function hide_header() {
		return false;
	}
	
	public function cron() {

		// If using external database, sync the Moodle table with it
        if(get_config('term_calendar', 'extdb') == 1) {
            require_once('lib.php');
            syncdb();
        }
		
		return true;
	}
	
	public function html_attributes() {
	    $attributes = parent::html_attributes(); // Get default values
	    $attributes['class'] .= ' block_'. $this->name(); // Append our class to class attribute
	    return $attributes;
	}

	public function applicable_formats() {
		return array('site' => true);
	}

    function get_content() {

        if ($this->content !== null) {
          return $this->content;
        }

        global $DB;
        require_once('lib.php');

        $this->content = new stdClass;


        ////////////////////////////////////////
        // THIS NEEDS TO BE PROPERLY CODED USING get_string, etc
        // ERROR MESSAGE SHOULD PROVIDE LINK TO DB SETTINGS
        ////////////////////////////////////////
        if(!is_configured()) {
            $this->content->text = 'Your external database has not been configured.';
            return $this->content;
        }

        //get current term information
        $today = time();
        if($curterm = $DB->get_record_select('block_term_calendar', 'enddate > '.$today.' AND startdate < '.$today))
        {
            $this->content->text .= html_writer::start_tag('h2');
            $this->content->text .= $curterm->name;
            $this->content->text .= html_writer::end_tag('h2');
            $this->content->text .= html_writer::start_tag('p');
            $this->content->text .= date('M. j, Y', $curterm->startdate).' - '.date('M. j, Y', $curterm->enddate);
            $this->content->text .= html_writer::end_tag('p');
        }else{
            $this->content->text   = get_string('noterms', 'block_term_calendar');
        }

        $url = new moodle_url('/blocks/term_calendar/view.php');
        $this->content->text .= html_writer::start_tag('p')
								.html_writer::link($url, get_string('addterm', 'block_term_calendar'))
								.html_writer::end_tag('p');
								
        $dbinfo = new moodle_url('/admin/settings.php?section=blocksettingterm_calendar');
        $this->content->footer = html_writer::link($dbinfo, get_string('admin', 'block_term_calendar'));
        return $this->content;
  	}

    public function specialization() {
        $this->title = get_string('currentterm', 'block_term_calendar');
		if(!get_config('term_calendar', 'mapterm')) {
        	set_config('mapterm', get_string('maptermdef', 'block_term_calendar'), 'term_calendar');
		}			

        // If using external database, sync the Moodle table with it
        if(get_config('term_calendar', 'extdb') == 1) {
            require_once('lib.php');
            //////////////////////////////////
            // GENERATING AN ERROR ON UNINSTALL
            //////////////////////////////////
           syncdb();
        }
	}
}
