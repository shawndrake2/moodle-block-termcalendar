term_calendar
=============

Moodle 2.4 Term Calendar block

Developed by Shawn Drake

Tracks term information. Does not need to have an instance
installed in order to function. 

Can be (and probably should be) used as a dependancy for 
other plugins to dynamically deliver content to users in 
the Moodle 2.4 environment.
